package com.example.kasus2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SpinnerAvtivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_avtivity);

        Bundle extras = getIntent().getExtras();
        String spin1 = extras.getString("spin1");
        String spin2 = extras.getString("spin2");

        TextView dataSpin1 = (TextView) findViewById(R.id.dataSpin1);
        TextView dataSpin2 = (TextView) findViewById(R.id.dataSpin2);

        CheckBox checkBox1 = findViewById(R.id.checkBox);
        CheckBox checkBox2 = findViewById(R.id.checkBox2);
        CheckBox checkBox3 = findViewById(R.id.checkBox3);
        CheckBox checkBox4 = findViewById(R.id.checkBox4);

        Button btnSave = (Button) findViewById(R.id.buttonSave);
        Button btnBack = (Button) findViewById(R.id.buttonBack);

        dataSpin1.setText(spin1);
        dataSpin2.setText(spin2);

        if(spin1.equals("Bangsal Anak")){
            switch (spin2){
                case "Demam Berdarah":
                    checkBox1.setText("Temperatur kurang dari 37");
                    checkBox2.setText("Tingkat haemogoblin di atas 35000");
                    checkBox3.setText("Mata tidak cekung");
                    checkBox4.setText("Aktif");
                    break;
                case "Tipus":
                    checkBox1.setText("Temperatur kurang dari 37");
                    checkBox2.setText("Tingkat leukosit normal");
                    checkBox3.setText("Tidak diare");
                    checkBox4.setText("Nafsu makan kembal");
                    break;
            }
        }else if(spin1.equals("Bangsal Dewasa")){
            switch (spin2){
                case "Demam Berdarah":
                    checkBox1.setText("Temperatur kurang dari 36,5");
                    checkBox2.setText("Tingkat haemogoblin di atas 30000");
                    checkBox3.setText("Mata tidak cekung");
                    checkBox4.setText("Aktif");
                    break;
                case "Tipus":
                    checkBox1.setText("Temperatur kurang dari 37");
                    checkBox2.setText("Tingkat leukosit normal");
                    checkBox3.setText("Tidak diare");
                    checkBox4.setText("Nafsu makan kembal");
                    break;
            }
        }


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int len = 0;
                List<String> arrlist = new ArrayList<String>();
                if(checkBox1.isChecked()) {
                    len++;
                    arrlist.add(checkBox1.getText().toString());
                }
                if(checkBox2.isChecked()) {
                    len++;
                    arrlist.add(checkBox2.getText().toString());
                }
                if(checkBox3.isChecked()) {
                    len++;
                    arrlist.add(checkBox3.getText().toString());
                }
                if(checkBox4.isChecked()) {
                    len++;
                    arrlist.add(checkBox4.getText().toString());
                }
                String[] arr = new String[len+1];
                if (len == 0){
                    arr[0] = "no disease detected";
                }else{
                    arr = arrlist.toArray(arr);
                }

                saveToFile(spin1, spin2, arr);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToMain();
            }
        });
    }

    public void saveToFile(String spin1, String spin2, String[] symptoms){
        TextView dataSpin1 = (TextView) findViewById(R.id.dataSpin1);

        try {
            File fl = new File("/data/data/com.example.kasus2","data.txt");
            if (fl.createNewFile()) {
                System.out.println("File Created: " + fl.getName());
            }

            if(fl.exists()){
                FileWriter fileWrite = new FileWriter(fl,true);
                fileWrite.append(spin1 + "|" + spin2);
                for (int i = 0 ; i < symptoms.length-1 ; i++){
                    fileWrite.append("|" + symptoms[i]);
                }
                fileWrite.append("\n");
                fileWrite.close();
                System.out.println(fl.getAbsolutePath());
                System.out.println("Successfully wrote to the file.");
            }

            Toast.makeText(getBaseContext(), "File saved successfully!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            System.out.println("An error occured.");
            e.printStackTrace();
        }
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void backToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}